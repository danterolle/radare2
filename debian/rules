#!/usr/bin/make -f
DEB_HOST_MULTIARCH ?= $(shell dpkg-architecture -qDEB_HOST_MULTIARCH)

EXTRA_FLAGS=-Duse_sys_capstone=true
EXTRA_FLAGS+= -Duse_sys_magic=true
EXTRA_FLAGS+= -Duse_sys_zip=true
EXTRA_FLAGS+= -Duse_sys_zlib=true
EXTRA_FLAGS+= -Duse_sys_lz4=true
EXTRA_FLAGS+= -Duse_sys_xxhash=true
EXTRA_FLAGS+= -Duse_webui=false
EXTRA_FLAGS+= -DSOURCE_DATE_EPOCH=${SOURCE_DATE_EPOCH}

DEB_BUILD_MAINT_OPTIONS = hardening=+bindnow
DPKG_EXPORT_BUILDFLAGS = 1
include /usr/share/dpkg/buildflags.mk

# architectures with debugging support
DEBUG_SUPPORT=0
debug_architectures := i386 amd64 arm arm64 mips mips64 powerpc ppc64
ifeq ($(DEB_HOST_ARCH_CPU),$(findstring $(DEB_HOST_ARCH_CPU),$(debug_architectures)))
DEBUG_SUPPORT=1
endif

# kfreebsd and hurd are not supported
ifneq (linux,$(DEB_HOST_ARCH_OS))
DEBUG_SUPPORT=0
endif

ifeq (0,$(DEBUG_SUPPORT))
EXTRA_FLAGS+= -Ddebugger=false
else
EXTRA_FLAGS+= -Ddebugger=true
endif

%:
	dh $@ --buildsystem=meson

override_dh_auto_configure:
	dh_auto_configure -- ${EXTRA_FLAGS}

override_dh_auto_test:
	# skip tests

override_dh_makeshlibs:
	dh_makeshlibs --exclude=/usr/lib/$(DEB_HOST_MULTIARCH)/radare2/

override_dh_auto_install:
	dh_auto_install
	# install zsh completion files
	mkdir -p debian/tmp/usr/share/zsh/vendor-completions
	install -m 644 doc/zsh/_* debian/tmp/usr/share/zsh/vendor-completions
	# Remove incorrect executable flag
	chmod a-x debian/tmp/usr/share/radare2/*/magic/*

override_dh_missing:
	dh_missing --fail-missing
